from SPARQLWrapper import SPARQLWrapper, JSON
from nltk.corpus import wordnet
import pandas
from rdflib import Graph, Literal, URIRef, Namespace
from rdflib.namespace import XSD


def main():
    # parameters
    input_file = 'etape3_(try)plat.tet789_eng.csv'
    spaqrl_file = "http://localhost:3030/lemon/sparql"
    output_file = 'etape4_(try)plat.tet789_eng.nt'

    Data = pandas.read_csv(input_file)
    nmbr_lines = len(Data)

    global sparql
    sparql = SPARQLWrapper(spaqrl_file)

    graph = Graph()
    for line in range(nmbr_lines):
        lemr = Lemon_synset(Data["Synset"][line])
        lemr = URIRef(lemr)
        urn = Namespace("urn:absolute:ontosenticnet#")
        sentic_concept = Data["Concept"][line].replace("https://sentic.net/api/en/concept/", "")
        graph.add((lemr, urn.text, Literal(sentic_concept)))
        graph.add((lemr, urn.pleasantness, Literal(Data["Pleasantness"][line], datatype=XSD.decimal)))
        graph.add((lemr, urn.attention, Literal(Data["Attention"][line], datatype=XSD.decimal)))
        graph.add((lemr, urn.sensitivity, Literal(Data["Sensitivity"][line], datatype=XSD.decimal)))
        graph.add((lemr, urn.aptitude, Literal(Data["Aptitude"][line], datatype=XSD.decimal)))

    graph.serialize(destination=output_file, format="nt")
    print("\n The ontology was saved as:", output_file)


def Lemon_synset(synset):
    # sysnet of wordnet
    synset = wordnet.synset(synset)
    # format of a word
    lemma_name = synset.lemmas()[0].key()
    # word type
    lemma_type = getType(synset.pos())
    query = """
        PREFIX uby: <http://purl.org/olia/ubyCat.owl#>

        SELECT ?uri
        WHERE {
          ?uri uby:externalReference "[POS: %s] %s"
        }
    """ % (lemma_type, lemma_name)

    sparql.setQuery(query)
    sparql.setReturnFormat(JSON)
    results = sparql.query().convert()
    result = results['results']['bindings'][0]
    if result:
        return result['uri']['value'].split("#")[0]
    else:
        return False


def getType(type):
    #Returns notation type
    if type == 'n':
        type = "noun"
    elif type == 'v':
        type = "verb"
    elif type == 'r':
        type = "adverb"
    else:
        type = "adjective"

    return type


if __name__ == '__main__':
    main()
