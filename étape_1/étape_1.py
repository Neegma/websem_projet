from bs4 import BeautifulSoup
import json
#install bs4 and lxml parser

def main():
    # Input
    tei_doc = "plat.tet789_eng.xml"
    first_stone = '281a'
    out_data = "step1_"+tei_doc.replace("xml", "json")

    with open(tei_doc, 'r') as tei:
        soup = BeautifulSoup(tei, 'lxml')
    # Deleting extra tags
    for note in soup.find_all("note"):
        note.extract()
    for object in soup.find_all():
        if not object.get("unit") == "section" and len(object.get_text(strip=True)) == 0:
            object.extract()

    # Initialize json
    JSON = {}
    JSON['titre'] = soup.title.text
    JSON['auteur'] = soup.monogr.author.text
    JSON['Repliques'] = []
    old_stone = first_stone
    text = ""

    for speaker in soup.find_all("speaker"):
        # Get the phrase related to speaker
        soup_contents = speaker.find_next_sibling().contents
        for content in soup_contents:
            try:
                # Get the number of milestone
                new_stone = content.get("n")
                # Check if a phrase is on the same line with the same milestone
                if new_stone is not None:
                    # Json update if new milestone exists
                    JSON['Repliques'].append({'Personnage': speaker.text, 'Position': old_stone, 'Texte': text})
                    old_stone = new_stone
                    text = ""
                else:
                    # Json update if old milestone
                    JSON['Repliques'].append({'Personnage': speaker.text, 'Position': old_stone, 'Texte': text})
                    text = ""
            except AttributeError:
                # clean text till next milestone and concatenate
                text += content.replace('\n', ' ').replace('  ', '')

    with open(out_data, "w") as out:
        json.dump(JSON, out, indent=4)

if __name__ == '__main__':
    main()
