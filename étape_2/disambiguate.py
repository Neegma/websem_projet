from pywsd import disambiguate
from pywsd.similarity import max_similarity as maxsim
import pandas
from tqdm import tqdm


def main():

    # parameters
    input_file = 'etape_1(try)plat.tet789_eng.json'
    input_text = 'Texte'
    output_wsd = 'WSD'
    output_file = "etape_2(try)plat.tet789_eng.json"


    json = pandas.read_json(input_file, typ="series")
    # count number of segments
    nmbr_segments = len(json.Repliques)
    x = 0

    with tqdm(total=nmbr_segments, leave=False) as pbar:
        while x <= nmbr_segments:
            text = json.Repliques[x][input_text]
            try:
                WSD = disambiguate(
                    text,
                    algorithm=maxsim,
                    similarity_option='wup',
                    keepLemmas=True)
                wsd2 = list()
                for word in WSD:
                    wsd2.append((word[0], word[1], word[2].name(
                    ) if word[2] is not None else word[2]))

                json.Repliques[x][output_wsd] = wsd2
                output_filename = open(
                    output_file,
                    "w",
                    encoding='utf-8')
                output_filename.write(json.to_json())
                output_filename.close()
                pbar.update(1)
            except IndexError:
                print("Segment error", x + 1)
                pbar.update(1)
            x += 1



    print("\nJSON file with WSD was created", output_file)


if __name__ == '__main__':
    main()
