from SPARQLWrapper import SPARQLWrapper, JSON
from nltk.corpus import wordnet
from os import path
import pandas
import json
from itertools import product
from tqdm import tqdm


def main():

    # parameters
    input_file = "etape_2(try)plat.tet789_eng.json"
    sparql = "http://localhost:3030/ontosenticnet2/sparql"
    repliques = 'Repliques'
    disembiguated_r = 'WSD'
    output_file = "etape3_(try)plat.tet789_eng.csv"

    Json = pandas.read_json(input_file, typ="series")

    output_synset = 'step3_RepliqueSynsetDict.json'
    global repl_syn_dict
    repl_syn_dict = {}
    if path.exists(output_synset):
        with open(output_synset, "r", encoding='utf-8') as exisit_synset_dict:
            repl_syn_dict = json.loads(exisit_synset_dict.read())

    global best_similarity
    best_similarity = []
    output_sim_list = 'etape3_RepSimList.txt'
    sub_synset_list = []
    final_result = []
    global Sparql
    Sparql = SPARQLWrapper(sparql)

    if repl_syn_dict == {}:
        for replique in Json[repliques]:
            try:
                for word in replique[disembiguated_r]:
                    synset = word[2]
                    if synset and synset not in sub_synset_list:
                        sub_synset_list.append(synset)
                        SigmaSemanticNet(synset)
            except KeyError:
                continue
            # intermidiate result
            with open(output_synset, "w", encoding='utf-8') as output_synset:
                json.dump(repl_syn_dict, output_synset)
    else:
        print(
            "\nThe dictionary", output_synset, "already exists")

    if best_similarity == []:
        with tqdm(total=len(repl_syn_dict), leave=False) as pbar:
            for firstSyns in repl_syn_dict:
                sementic_syns_list = []
                for sementic in repl_syn_dict[firstSyns]:
                    semSyns = wordnet.synsets(sementic)
                    for sem in semSyns:
                        sementic_syns_list.append(sem)
                best_similarities = find_best_sim(
                                        [wordnet.synset(firstSyns)],
                                        sementic_syns_list,
                                        repl_syn_dict[firstSyns])
                if best_similarities:
                    best_similarity.append(best_similarities)
                pbar.update(1)
        # Intermediate result
        with open(output_sim_list, "w", encoding='utf-8') as OUTSimList:
            OUTSimList.write('\n'.join(str(x) for x in best_similarity))
        print(
            "\nThe list of triples saved as",
            output_sim_list)

    # Looking for pleasantness attention sensitivity aptitude
    for value in best_similarity:
        word = value[2].name().split(".")[0]
        sentics = getSenticsValue(word)
        if sentics:
            output = {
                "Similarity": value[0],
                "Synset": value[1].name(),
                "Concept": "https://sentic.net/api/en/concept/"+word,
            }
            for sentic in sentics:
                output[sentic] = sentics[sentic]
            final_result.append(output)

    DataFrame = pandas.DataFrame(final_result)
    DataFrame.to_csv(output_file)
    print("Analyses done:", output_file)


def SigmaSemanticNet(synset):

    synsetlist = []
    word = synset.split(".")[0]
    query = """
        prefix urn: <urn:absolute:ontosenticnet#>

        SELECT ?sementics
        WHERE {
            ?text urn:text "%s".
            ?text urn:semantics ?sementics
        }
    """ % word
    # sparql queries
    try:
        Sparql.setQuery(query)
        Sparql.setReturnFormat(JSON)
        results = Sparql.query().convert()
    except:
        print("Check if the database is connected")
        exit()

    for result in results['results']['bindings']:
        synsetlist.append(result['sementics']['value'].split("#")[1])
    if synsetlist != []:
        repl_syn_dict[synset] = synsetlist


def find_best_sim(term1, term2, word_missing):

    try:
        best = max((wordnet.wup_similarity(s1, s2) or 0, s1, s2) for s1, s2 in
                    product(term1, term2))
    except ValueError:
        print("Value error")

    if 'best' in locals():
        best = list(best)
        if best[0]:
            return best


def getSenticsValue(word):
    Ontosentic_val = {}
    word = word.replace("_", " ")
    query = """
        prefix urn: <urn:absolute:ontosenticnet#>

        SELECT ?Pleasantness ?Attention ?Sensitivity ?Aptitude
        WHERE {
            ?text urn:text "%s".
            ?text urn:pleasantness ?Pleasantness.
            ?text urn:attention ?Attention.
            ?text urn:sensitivity ?Sensitivity.
            ?text urn:aptitude ?Aptitude
        }
    """ % word
    # sparql queries
    Sparql.setQuery(query)
    Sparql.setReturnFormat(JSON)
    results = Sparql.query().convert()
    for result in results['results']['bindings']:
        for key in result:
            Ontosentic_val[key] = result[key]['value']
    return Ontosentic_val


if __name__ == '__main__':
    main()
