****Plato_Homer_Personality****

**Context**

The current project is proposed by Davide Picca, the professor of semantic web course. The goal of this project is to describe emotional-psychological profile
of the characters in Plato's dialogues in Hippias Major, Hippias Minor, Ion, Menexenus, Cleitophon, Timaeus, Critias, Minos, Epinomis (plat.tet789_eng.xml file).

**Objectives**

1. Extract title, author's name, speaker's name, milestone value and text corresponging to each speaker in the form of nested dictionary. Then save the output in .json format.
2. Disambiguate the text using pywsd library.
3. Compare the disambiaguated text with OntoSenticNet data. 
4. Make ontological alignment with LemonUby data.

**Development and procedure**

The first folder étape_1 contains the input file plat.tet789_eng.xml, the python code étape_1.py and the output etape_1(try)plat.tet789_eng.json file.
The second folder étape_2 contains disambiguate.py, etape_1(try)plat.tet789_eng.json as an input file and etape_2(try)plat.tet789_eng.json as an output file 
with added WSD key. The third folder étape_3 contains etape3.py, intermidiate result files etape3_repl_syn_dict.json, etape3_repl_syn_list.txt and output file
etape_3(try)plat.tet789_eng.csv. The forth folder étape_4 has étape4.py, input and output files etape_3(try)plat.tet789_eng.csv and etape_4(try)plat.tet789_eng.nt respectively.

In order to make the code execute correctly, ontosenticnet.owl and wn.net shall be uploader onto Fuseki server. For me only version 3.13.1 of Apache Jena Fuseki could run correctly
with java previously installed. 

Also, the following list of python libraries should be installed:
- bs4
- lxml parser
- pywsd
- pandas
- tqdm
- SPARQLWrapper
- ntlk
- intertools

Etape_1

Etape_1.py uses BeautifulSoup and lxml parser to go through the input file plat.tet789_eng.xml and find all <speaker> tags. Then, it returns the content of each <speaker> tag.
The content of each speaker also has milestone and text that we append to the dictionary. Also, the first line of text contains two milestones, whereas all other lines contain
only one. Hence, I have started from the second milestone on the first line: first_stone = '281a'.

Etape_2

The second step is made with disambiguate.py file which uses ntlk and pywsd to disabiguate the text value and update the dictionary with the key:value as WSD:list
of disambiguated words. The function disambiguate uses max_similarity algorithm which caused the following error: ``IndexError: list index out of range ``. 
After finding the solution for this problem, the code was working correctly. 

Etape_3

In the third step, we need to find for each synset obtained in etape_2 the best corresponding concept or semantic from OntoSenticNet uploaded on Fuseki server. On my localhost
it was saved as ontosenticnet2. So, in etape3.py it is used as "http://localhost:3030/ontosenticnet2/sparql". Then, it only remains to recover the sentics with the most similar
concept. For each synset in etape_2(try)plat.tet789_eng.json we try to get the concepts in OntoSenticNet, then get the synsets of these concepts, calculate the similarity
between the original synset and the synsets of the OntoSenticNet concepts. After that, we need to return the most similar concept. In the end, for each selected concept,
we recover the corresponding sentics and prepare the output as etape_3(try)plat.tet789_eng.csv. 

The output file has the following structure: 
```
``Similarity,Synset,Concept,Pleasantness,Attention,Sensitivity,Aptitude
0,1.0 ,interfering.s.01,https://sentic.net/api/en/concept/interfering,-0.99,0,0.851,-0.89
1,0.2222222222222222,travel.v.01,https://sentic.net/api/en/concept/explorer,0.069,0.07,0,0.116
2,0.4,state.n.02,https://sentic.net/api/en/concept/democracy,0.044,0.133,-0.08,0.087
`` 
```


Etape_4

The last step consists of making an ontological alignment between LemonUby and etape_3(try)plat.tet789_eng.csv. On my localhost LemonUby was saved as 
"http://localhost:3030/lemon/sparql". 

The output file etape_4(try)plat.tet789_eng.nt becomes as follows:
> ``<http://lemon-model.net/lexica/uby/wn/WN_Sense_20141> <urn:absolute:ontosenticnet#text> "speech" .
> <http://lemon-model.net/lexica/uby/wn/WN_Sense_69653> <urn:absolute:ontosenticnet#sensitivity> "0.0"^^<http://www.w3.org/2001/XMLSchema#decimal> .
> <http://lemon-model.net/lexica/uby/wn/WN_Sense_63000> <urn:absolute:ontosenticnet#pleasantness> "0.055999999999999994"^^<http://www.w3.org/2001/XMLSchema#decimal> .
> <http://lemon-model.net/lexica/uby/wn/WN_Sense_166013> <urn:absolute:ontosenticnet#attention> "0.461"^^<http://www.w3.org/2001/XMLSchema#decimal> .
> <http://lemon-model.net/lexica/uby/wn/WN_Sense_95758> <urn:absolute:ontosenticnet#pleasantness> "-0.89"^^<http://www.w3.org/2001/XMLSchema#decimal> .
> `` 


**Conclusion**

On every step of the project, the code executes correctly if all the requirements were satisfied. The final file etape_4(try)plat.tet789_eng.nt has a form of a database
encoded in N-triples as it is mentioned in the work of other students. Hence, after uploading the final ontolody in Fuseki, we can search for a specific word and find the
corresponding concepts from Lemon as well as the values from OntoSenticNet which makes the understanding of semantics of Plato's text more complete.

Example of searching for word "kindness" and associated aptitude value:

> PREFIX urn:<urn:absolute:ontosenticnet#>
> 
> SELECT *
> WHERE {
>   ?lemon_sens urn:text "kindness".
>   ?lemon_sens urn:aptitude ?aptitude_value
> }



```
|lemon_sens                                             |aptitude_value      |

|<http://lemon-model.net/lexica/uby/wn/WN_Sense_172530> |"0.98"^^xsd:decimal |
```





















